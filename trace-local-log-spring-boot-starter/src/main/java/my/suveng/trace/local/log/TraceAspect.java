package my.suveng.trace.local.log;

import cn.hutool.core.util.IdUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.TtlMDCAdapter;
import org.springframework.stereotype.Component;

/**
 * @author suwenguang
 * @date 2020/11/6 9:55 上午
 * @since 1.0
 */
@Aspect
@Component
public class TraceAspect {
    /**
     * 切面方法,用于接口切面AOP增强
     * @author suwenguang
     * @date 2019-09-25
     */
    @Around(value = "execution(* *..*.*.*(..)) && @annotation(trace) ", argNames = "pjp,trace")
    public Object aroundMethod(ProceedingJoinPoint pjp, Trace trace) throws Throwable {
        String uuid = IdUtil.fastSimpleUUID();
        try {
            TtlMDCAdapter.getInstance().put("trace_local_id", uuid);
            return pjp.proceed();
        } finally {
            TtlMDCAdapter.getInstance().clear();
        }
    }
}
