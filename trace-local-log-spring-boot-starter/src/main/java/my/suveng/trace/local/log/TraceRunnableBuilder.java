package my.suveng.trace.local.log;

import org.slf4j.TtlMDCAdapter;

/**
 *
 * @author suwenguang
 * @date 2020/11/6 10:06 上午
 * @since 1.0
 */
public class TraceRunnableBuilder {

    public static Runnable build(Runnable runnable) {
        return () -> {
            runnable.run();
            // 清除mdc
            TtlMDCAdapter.getInstance().clear();
        };
    }
}
