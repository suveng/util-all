package my.suveng.leetcode.stack;

import java.util.Stack;

/**
 * 用栈实现队列
 * @author suwenguang
 **/
public class S232 {
    public static void main(String[] args) {
        MyQueue queue = new MyQueue();

        queue.push(1);
        queue.push(2);
        System.out.println(queue.peek());
        System.out.println(queue.pop());
        System.out.println(queue.empty());
    }

    static class MyQueue {
        public final Stack<Integer> in = new Stack<>();
        public final Stack<Integer> out = new Stack<>();

        /** Initialize your data structure here. */
        public MyQueue() {

        }

        /** Push element x to the back of queue. */
        public void push(int x) {
            if (!in.isEmpty() && out.isEmpty()) {
                in.push(x);
                return;
            }
            while (!out.isEmpty()) {
                in.push(out.pop());
            }
            in.push(x);
        }

        /** Removes the element from in front of queue and returns that element. */
        public int pop() {
            if (!out.isEmpty() && in.isEmpty()) {
                return out.pop();
            }

            while (!in.isEmpty()) {
                out.push(in.pop());
            }
            return out.pop();
        }

        /** Get the front element. */
        public int peek() {
            if (!out.isEmpty() && in.isEmpty()) {
                return out.peek();
            }

            while (!in.isEmpty()) {
                out.push(in.pop());
            }
            return out.peek();
        }

        /** Returns whether the queue is empty. */
        public boolean empty() {
            return in.isEmpty() && out.isEmpty();
        }
    }

}

