package my.suveng.leetcode.stack;

import java.util.ArrayDeque;

/**
 * 用队列实现栈
 * @author suwenguang
 **/
public class S225 {
    public static void main(String[] args) {
        MyStack obj = new MyStack();
        obj.push(2);
        int param_2 = obj.pop();
        int param_3 = obj.top();
        boolean param_4 = obj.empty();
    }


    static class MyStack {
        private final ArrayDeque<Integer> deque = new ArrayDeque<>();

        /** Initialize your data structure here. */
        public MyStack() {

        }

        /** Push element x onto stack. */
        public void push(int x) {
            deque.push(x);
        }

        /** Removes the element on top of the stack and returns that element. */
        public int pop() {
            return deque.pop();
        }

        /** Get the top element. */
        public int top() {
            if (deque.isEmpty()) {
                return 0;
            }
            return deque.peek();
        }

        /** Returns whether the stack is empty. */
        public boolean empty() {
            return deque.isEmpty();
        }
    }
}

