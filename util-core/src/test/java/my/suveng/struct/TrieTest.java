package my.suveng.struct;

import cn.hutool.core.util.RandomUtil;
import my.suveng.struct.trie.Trie;
import my.suveng.util.json.Jackson;
import my.suveng.util.server.MemoryUtil;
import my.suveng.util.server.ServerUtil;
import org.junit.Test;

import java.util.List;

/**
 *
 * @author suwenguang
 **/
public class TrieTest {

    @Test
    public void name() {
        Trie trie = new Trie();
        trie.insert("hello");
        trie.insert("hello1");
        trie.insert("hello2");
        trie.insert("hello3");
        trie.insert("hello4");
        trie.insert("hello5");
        trie.insert("hello6");
        trie.insert("hello7");

        trie.insert("to");
        trie.insert("he");
        trie.insert("she");

        System.out.println(trie.get("he"));
        System.out.println(trie.get("to"));
        System.out.println(trie.get("she"));
        System.out.println(trie.get("he"));
        System.out.println(trie.get("hello"));
        System.out.println(trie.get("hed"));

        System.out.println(trie.toString());


        System.out.println("删除");
        trie.delete("he");
        System.out.println(trie.toString());

        System.out.println("获取字典");
        System.out.println(trie.startWith("he"));
        List<String> stringList = trie.trieStartWith("he");
        System.out.println(Jackson.toJsonString(stringList));

        System.out.println("判断");
        System.out.println(trie.existStartWith("he"));
        System.out.println(trie.exist("he"));

    }


    @Test
    public void zhcn() {
        Trie trie = new Trie();

        trie.insert("中文");
        trie.insert("中国");
        trie.insert("中央");
        trie.insert("中国人");
        trie.insert("中国号");


        System.out.println(trie.toString());
        System.out.println(trie.existStartWith("中国"));
        System.out.println(trie.exist("中国"));
        trie.delete("中文");
        System.out.println(trie.toString());
        System.out.println(trie.existStartWith("中文"));

    }

    @Test
    public void delete() throws InterruptedException {
        System.out.println(ServerUtil.getProcessId());
        Trie trie = new Trie();

        for (int i = 0; i < 10000; i++) {
            String s = RandomUtil.randomNumbers(5);
            trie.insert(s);
        }

        MemoryUtil.all(trie);

        for (int i = 0; i < 10000; i++) {
            trie.delete(RandomUtil.randomNumbers(5));
        }

        MemoryUtil.all(trie);

        System.out.println(trie.countNode());
        trie.removeRedundantNode();
        System.out.println(trie.countNode());


        MemoryUtil.all(trie);

    }

    public static void main(String[] args) throws InterruptedException {
        TrieTest trieTest = new TrieTest();
        trieTest.delete();
    }
}
