package my.suveng.struct.heap;

import org.junit.Test;

/**
 *
 * @author suwenguang
 **/
public class MaxHeapTest {

    @Test
    public void newMaxHeap() {
        MaxHeap maxHeap = new MaxHeap(10);

        maxHeap.push(new HeapElement(123));
        maxHeap.push(new HeapElement(12));
        maxHeap.push(new HeapElement(0));
        maxHeap.push(new HeapElement(-2));
        maxHeap.push(new HeapElement(-100));

        System.out.println(maxHeap);

        maxHeap.pop();
        maxHeap.pop();
        System.out.println(maxHeap);

    }
}
