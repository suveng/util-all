package my.suveng.model.common.response;

import my.suveng.model.common.interfaces.response.IMessage;
import my.suveng.model.common.response.expand.demo.ExpandDemoBuilder;
import my.suveng.util.json.Jackson;
import org.junit.Test;

import java.util.HashMap;

/**
 *
 * @author suwenguang
 **/
public class MessageTest {
    @Test
    public void methodMessage() {
        HashMap<String, Object> data = new HashMap<>();
        data.put("name", "zhansan");
        data.put("age", 20);
        System.out.println(Jackson.toJsonString(Message.build(true, "method", data)));
    }

    @Test
    public void httpMessage() {
        HashMap<String, Object> data = new HashMap<>();
        data.put("name", "zhansan");
        data.put("age", 20);
        Message.configureMessageBuilder(ExpandDemoBuilder.class);
        IMessage<HashMap<String, Object>> method = Message.build(true, "method", data);
        System.out.println(Jackson.toJsonString(method));
        IMessage<HashMap<String, Object>> method1 = Message.build(true, "method", data, 1, 2);
        System.out.println(Jackson.toJsonString(method1));

        IMessage<HashMap<String, Object>> method2 = Message.build(true, "method", data);
        System.out.println(Jackson.toJsonString(method2));


    }
}
