package my.suveng.struct.trie;

import java.util.List;

/**
 * 字典树,前缀树Trie接口
 * @author suwenguang
 **/
public interface ITrie<K> {

    /**
     * 搜索字典
     */
    ITrieNode get(K key);


    /**
     * 插入字典
     */
    boolean insert(K key);

    /**
     * 删除字典
     */
    void delete(K key);

    /**
     * 清除冗余节点
     */
    void removeRedundantNode();

    /**
     * 是否存在某个字典
     */
    boolean exist(K key);

    /**
     * 是否存在以某个前缀开始的字典
     */
    boolean existStartWith(K key);

    /**
     * 是否某个节点下面有字典
     */
    boolean existKey(ITrieNode node);

    /**
     * 获取以某个前缀开始的节点列表, 属于中间节点
     */
    ITrieNode startWith(K key);

    /**
     * 获取以某个前缀开始的字典
     */
    List<String> trieStartWith(K key);

    /**
     * 全部字典
     */
    List<String> allToString();

    /**
     * 节点总数
     */
    int countNode();


}
