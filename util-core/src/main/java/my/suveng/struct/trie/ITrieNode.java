package my.suveng.struct.trie;

import java.util.List;

/**
 * 字典树/前缀树节点
 * @author suwenguang
 **/
public interface ITrieNode {

    List<ITrieNode> getLinks();

    char getValue();

    ITrieNode hasLink(char key);

    ITrieNode insertLink(char key);

    void setEnd(boolean flag);

    void setValue(char key);

    boolean isEnd();

}
