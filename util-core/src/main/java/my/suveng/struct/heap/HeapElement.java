package my.suveng.struct.heap;

/**
 *
 * @author suwenguang
 **/
public class HeapElement extends AbstractHeapElement {
    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public int compareTo(IHeapElement o) {
        if (o instanceof HeapElement) {
            HeapElement other = (HeapElement) o;
            return Integer.compare(this.value, other.getValue());
        }

        throw new RuntimeException("类型错误");
    }

    public HeapElement(int value) {
        this.value = value;
    }
}
