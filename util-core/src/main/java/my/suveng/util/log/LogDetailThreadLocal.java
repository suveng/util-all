package my.suveng.util.log;

import com.alibaba.ttl.TransmittableThreadLocal;

/**
 * 线程私有的日志抽象封装
 * @author suwenguang
 */
public class LogDetailThreadLocal {

    // 使用线程传递的threadlocal
    public static TransmittableThreadLocal<LogDetail> logDetailThreadLocal = new TransmittableThreadLocal<LogDetail>() {
        @Override
        protected LogDetail initialValue() {
            return new LogDetail();
        }
    };

}

