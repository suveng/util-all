package my.suveng.util.log;

import lombok.Data;

@Data
public class LogDetail {
    private String threadName;
    private String tag;
    private String requsetId;


    public static LogDetail get() {
        return LogDetailThreadLocal.logDetailThreadLocal.get();
    }
}
