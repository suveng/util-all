package my.suveng.util.log;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.NOPLoggerFactory;

import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

public class PlusLogFactoryHutool extends LogFactory {

    public PlusLogFactoryHutool() {
        this(false);
    }

    private PlusLogFactoryHutool(boolean failIfNOP) {
        super("Slf4j");
        this.checkLogExist(LoggerFactory.class);
        if (failIfNOP) {
            final StringBuilder buf = new StringBuilder();
            PrintStream err = System.err;

            try {
                System.setErr(new PrintStream(new OutputStream() {
                    @Override
                    public void write(int b) {
                        buf.append((char) b);
                    }
                }, true, "US-ASCII"));
            } catch (UnsupportedEncodingException var8) {
                throw new Error(var8);
            }

            try {
                if (LoggerFactory.getILoggerFactory() instanceof NOPLoggerFactory) {
                    throw new NoClassDefFoundError(buf.toString());
                }

                err.print(buf);
                err.flush();
            } finally {
                System.setErr(err);
            }

        }
    }

    @Override
    public Log createLog(String name) {
        return new PlusLogHutool(name);
    }

    @Override
    public Log createLog(Class<?> clazz) {
        return new PlusLogHutool(name);
    }
}
