package my.suveng.util.bank;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import my.suveng.model.common.interfaces.define.StaticUtil;

/**
 * 银行卡工具
 * @author suwenguang
 **/
@StaticUtil
@Slf4j
public class BankCardUtil {

	/**
	 * 检查银行卡的真伪
	 *
	 * 从最右一位数开始向左，把每个数字交替乘1或2，
	 * 如果结果大于 9就减9。如果把各位数的计算结果加起来，
	 * 最后得到的总和能被10 整除，那这个卡号就是有效卡号。
	 * @author suwenguang
	 */
	public static boolean validateBankCard(String bankcard) {
		if (StrUtil.isEmpty(bankcard)) {
			log.info("银行卡为空");
			return false;
		}

		char[] bankChars = bankcard.toCharArray();
		int[] res = new int[bankChars.length];

		for (int i = bankChars.length - 1; i >= 0; i--) {
			int t = i % 2;
			String s = String.valueOf(bankChars[i]);
			int integer = Integer.parseInt(s);

			//从最右一位数开始向左，每个数字交替乘2
			if (t != 0) {
				integer = integer * 2;
			}

			if (integer > 9) {
				integer = integer - 9;
			}

			res[i] = integer;
		}

		int sum = 0;
		for (int re : res) {
			sum = sum + re;
		}

		return sum % 10 == 0;
	}
}
