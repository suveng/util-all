package my.suveng.util.server;

import my.suveng.model.common.interfaces.define.StaticUtil;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.InetAddress;

/**
 * 服务器相关工具
 * @author suwenguang
 **/
@StaticUtil
public class ServerUtil {

    public static Long getThreadId() {
        try {
            return Thread.currentThread().getId();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取当前进程id
     */
    public static Long getProcessId() {
        try {
            RuntimeMXBean runtime = ManagementFactory.getRuntimeMXBean();
            String name = runtime.getName();
            String pid = name.substring(0, name.indexOf('@'));
            return Long.parseLong(pid);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取当前服务器ip地址
     */
    public static String getServerIp() {
        try {
            //用 getLocalHost() 方法创建的InetAddress的对象
            InetAddress address = InetAddress.getLocalHost();
            return address.getHostAddress();
        } catch (Exception e) {
            return null;
        }
    }
}
