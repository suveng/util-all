package my.suveng.util.server;

import my.suveng.model.common.interfaces.define.StaticUtil;
import org.openjdk.jol.info.ClassLayout;
import org.openjdk.jol.info.GraphLayout;

/**
 * 内存工具
 * @author suwenguang
 **/
@StaticUtil
public class MemoryUtil {
    public static void all(Object o) {
        //输出内存占用统计
        System.out.println("=========================");
        System.out.println(GraphLayout.parseInstance(o).toFootprint());
    }

    public static void relative(Object o) {
        //输出对象相关所有内存占用
        System.out.println("=========================");
        System.out.println(GraphLayout.parseInstance(o).toPrintable());
    }


    public static void simple(Object o) {
        //打印对象内存占用
        System.out.println("=========================");
        System.out.println(ClassLayout.parseInstance(o).toPrintable());
    }
}
