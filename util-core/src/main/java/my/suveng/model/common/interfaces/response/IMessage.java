package my.suveng.model.common.interfaces.response;

/**
 * 消息抽象
 * @author suwenguang
 **/
public interface IMessage<T> {

    /**
     * 当前消息是否成功,用于业务判断
     * @author suwenguang
     */
    boolean isSuccess();

    /**
     * 获取系统返回的提示,最好是中文提示, 如果是国际版, 最好继承本接口,扩展该提示方法
     * @author suwenguang
     */
    String getMessage();

    /**
     * 获取消息返回的数据
     * @author suwenguang
     */
    T getData();

}
