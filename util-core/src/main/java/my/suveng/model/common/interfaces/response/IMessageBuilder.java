package my.suveng.model.common.interfaces.response;

import my.suveng.model.common.response.base.BaseMessageBuilder;

/**
 * 消息构建
 * @author suwenguang
 **/
public interface IMessageBuilder {

    /**
     * 构建消息
     * @author suwenguang
     */
    default <T> IMessage<T> build(boolean success, String message, T data) {
        return BaseMessageBuilder.INSTANCE.build(success, message, data);
    }

    default <T> IMessage<T> errorWithMessage(String message) {
        return BaseMessageBuilder.INSTANCE.errorWithMessage(message);

    }

    default <T> IMessage<T> successWithData(T data) {
        return BaseMessageBuilder.INSTANCE.successWithData(data);
    }

    /**
     * 扩展用的
     * @author suwenguang
     */
    default <T> IMessage<T> build(boolean success, String message, T data, Object... params) {
        return null;
    }
}
