package my.suveng.model.common.interfaces.response;

/**
 * 国际化错误码
 * @author suwenguang
 **/
public interface IInternationalCode extends ICode {
    /**
     * 获取错误码描述,中文
     * @author suwenguang
     */
    String getChineseMessage();
}
