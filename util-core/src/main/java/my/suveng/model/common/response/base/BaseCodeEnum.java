package my.suveng.model.common.response.base;

import my.suveng.model.common.interfaces.response.ICode;

/**
 * 返回码
 * @author suwenguang
 **/
public enum BaseCodeEnum implements ICode {
    //
    SUCCESS(0, "success"),
    FAIL(-1, "fail"),
    ;


    BaseCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    private Integer code;

    private String message;

    @Override
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
