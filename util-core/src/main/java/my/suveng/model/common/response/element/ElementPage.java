package my.suveng.model.common.response.element;

import lombok.Data;
import my.suveng.model.common.interfaces.response.IMessage;
import my.suveng.model.common.response.Message;
import my.suveng.model.common.response.base.BaseCodeEnum;

/**
 * 饿了么分页插件封装
 * @author suwenguang
 **/
@Data
public class ElementPage<T> {


    /**
     * 总数
     */
    private Long total;

    /**
     * 数据
     */
    private T list;


    public static <T> IMessage<ElementPage<T>> build(long total, T data) {
        ElementPage<T> elementPage = new ElementPage<>();
        elementPage.setTotal(total);
        elementPage.setList(data);
        return Message.build(true, BaseCodeEnum.SUCCESS.getMessage(), elementPage);
    }
}
