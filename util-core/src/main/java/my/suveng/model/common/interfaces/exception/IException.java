package my.suveng.model.common.interfaces.exception;

/**
 * 异常行为抽象
 * @author suwenguang
 **/
public interface IException {

    /**
     * 抛出异常
     * @author suwenguang
     */
    Exception throwOut(IExceptionEnum e);

}
