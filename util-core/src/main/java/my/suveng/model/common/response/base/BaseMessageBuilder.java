package my.suveng.model.common.response.base;

import my.suveng.model.common.interfaces.response.IMessageBuilder;

/**
 *
 * @author suwenguang
 **/
public class BaseMessageBuilder implements IMessageBuilder {
    public static BaseMessageBuilder INSTANCE = new BaseMessageBuilder();

    @Override
    public <T> BaseMessage<T> build(boolean success, String message, T data) {
        BaseMessage<T> result = new BaseMessage<>();
        result.setSuccess(success);
        result.setMessage(message);
        result.setData(data);
        return result;
    }


    @Override
    public <T> BaseMessage<T> errorWithMessage(String message) {
        BaseMessage<T> result = new BaseMessage<>();
        result.setSuccess(false);
        result.setMessage(message);
        result.setData(null);
        return result;
    }


    @Override
    public <T> BaseMessage<T> successWithData(T data) {
        BaseMessage<T> result = new BaseMessage<>();
        result.setSuccess(true);
        result.setMessage("success");
        result.setData(data);
        return result;
    }
}
