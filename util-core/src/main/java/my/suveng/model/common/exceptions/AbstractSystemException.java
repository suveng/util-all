package my.suveng.model.common.exceptions;

import my.suveng.model.common.interfaces.exception.IException;
import my.suveng.model.common.interfaces.exception.IExceptionEnum;
import my.suveng.model.common.interfaces.response.ICode;

/**
 * 统一异常封装
 * @author suwenguang
 */
public abstract class AbstractSystemException extends RuntimeException implements IExceptionEnum, IException {
    /**
     * 错误码
     * @see ICode#getCode()
     */
    protected Integer code;


    public AbstractSystemException() {
        super("");
        this.code = -1;
    }

    public AbstractSystemException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public AbstractSystemException(Integer code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public AbstractSystemException(Integer code, Throwable cause) {
        super(cause);
        this.code = code;
    }

    public AbstractSystemException(Integer code, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.code = code;
    }

    @Override
    public Integer getCode() {
        return null;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

}
