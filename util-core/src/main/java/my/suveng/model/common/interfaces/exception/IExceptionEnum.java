package my.suveng.model.common.interfaces.exception;

/**
 * 异常种类
 * @author suwenguang
 **/
public interface IExceptionEnum {

    /**
     * 获取异常码
     * @author suwenguang
     */
    Integer getCode();

    /**
     * 获取异常描述
     * @author suwenguang
     */
    String getMessage();

}
