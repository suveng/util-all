package my.suveng.model.common.response.expand.demo;

import cn.hutool.core.util.IdUtil;
import my.suveng.model.common.interfaces.response.IMessage;
import my.suveng.model.common.response.base.BaseCodeEnum;


/**
 * 公共类,可自行开发扩展
 * 返回给调用方
 * @author suwenguang
 */
public class ExpandDemo<T> implements IMessage<T> {
    /**
     * 错误码
     */
    private Integer code;

    /**
     * 业务追踪id
     */
    private String bId;


    /**
     * 标志, 业务判断
     */
    private boolean success;

    /**
     * 业务消息,系统透传,最好增加错误在后面,标识业务系统
     */
    private String message;

    /**
     * 消息数据,系统透传
     */
    private T data;

    public static <T> ExpandDemo<T> build(Integer code, String msg, T data) {
        ExpandDemo<T> result = new ExpandDemo<>();
        result.setCode(code);
        result.setMessage(msg);
        result.setData(data);
        result.setbId(IdUtil.fastSimpleUUID());
        return result;
    }

    public static <T> ExpandDemo<T> build(BaseCodeEnum resultEnum, T data) {
        ExpandDemo<T> result = new ExpandDemo<>();
        result.setCode(resultEnum.getCode());
        result.setMessage(resultEnum.getMessage());
        result.setData(data);
        result.setbId(IdUtil.fastSimpleUUID());
        return result;
    }

    public static <T> ExpandDemo<T> buildError(String msg) {
        ExpandDemo<T> result = new ExpandDemo<>();
        result.setCode(BaseCodeEnum.FAIL.getCode());
        result.setMessage(msg);
        result.setData(null);
        result.setbId(IdUtil.fastSimpleUUID());
        return result;
    }

    public static <T> ExpandDemo<T> buildSuccess(T data) {
        ExpandDemo<T> result = new ExpandDemo<>();
        result.setCode(BaseCodeEnum.SUCCESS.getCode());
        result.setMessage(BaseCodeEnum.SUCCESS.getMessage());
        result.setData(data);
        result.setbId(IdUtil.fastSimpleUUID());
        return result;
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getbId() {
        return bId;
    }

    public void setbId(String bId) {
        this.bId = bId;
    }

    @Override
    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
