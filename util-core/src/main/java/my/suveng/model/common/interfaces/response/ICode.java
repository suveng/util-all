package my.suveng.model.common.interfaces.response;

/**
 * 错误码
 * 0-1 系统编号
 * 2-3 系统版本
 * 3-4 功能模块
 * 5-10 具体错误码
 * @author suwenguang
 **/
public interface ICode {
    /**
     * 获取错误码
     * @author suwenguang
     */
    Integer getCode();


    /**
     * 获取错误码描述
     * @author suwenguang
     */
    String getMessage();
}
