package my.suveng.model.common.response.base;

import my.suveng.model.common.interfaces.response.IMessage;

public class BaseMessage<T> implements IMessage<T> {

    /**
     * 标志, 业务判断
     */
    private boolean success;

    /**
     * 业务消息,系统透传,最好增加错误在后面,标识业务系统
     */
    private String message;

    /**
     * 消息数据,系统透传
     */
    private T data;


    @Override
    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
