package my.suveng.model.common.response.expand.demo;

import cn.hutool.core.util.IdUtil;
import my.suveng.model.common.interfaces.response.IMessageBuilder;

/**
 * 扩展的demo
 * @author suwenguang
 **/
public class ExpandDemoBuilder implements IMessageBuilder {

    @Override
    public <T> ExpandDemo<T> build(boolean success, String message, T data, Object... params) {
        System.out.println("扩展的demo, 增加字段code, requestId");
        ExpandDemo<T> tOutputMessage = new ExpandDemo<>();
        if (params.length == 2) {
            //取第前两个, 第一个为 code , 第二个为requestId
            tOutputMessage.setCode((Integer) params[0]);
            tOutputMessage.setbId(params[1] + "");
        }
        tOutputMessage.setbId(IdUtil.fastSimpleUUID());
        tOutputMessage.setSuccess(success);
        tOutputMessage.setMessage(message);
        tOutputMessage.setData(data);
        return tOutputMessage;
    }


}
