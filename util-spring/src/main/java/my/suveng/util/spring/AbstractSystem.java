package my.suveng.util.spring;

import cn.hutool.core.lang.caller.CallerUtil;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import my.suveng.model.common.interfaces.define.SpringUtil;
import my.suveng.util.log.PlusLogFactoryHutool;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

/**
 * 抽象控制器
 * 封装通用方法
 * @author suwenguang
 **/
public abstract class AbstractSystem implements SpringUtil {
    //hutool日志
    private static final Log log = LogFactory.setCurrentLogFactory(new PlusLogFactoryHutool()).getLog(CallerUtil.getCaller().getName());

    @Autowired
    private Validator validator;

    public <T> void validate(T request) {
        Set<ConstraintViolation<T>> validate = validator.validate(request);
        if (validate.size() > 0) {
            for (ConstraintViolation<T> e : validate) {
                String msg = "参数校验不通过:" + e.getPropertyPath() + e.getMessage();
                log.error(msg);
                throw new RuntimeException(msg);
            }
        }
    }
}
