package my.suveng.util.dynamic.datatsource.annotations;

import java.lang.annotation.*;

/**
 * 选择数据源
 * @author suwenguang
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ChooseDatasource {
    DatasourceRoleType type() default DatasourceRoleType.master;
}
