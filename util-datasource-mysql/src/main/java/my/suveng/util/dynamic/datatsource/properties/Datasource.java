package my.suveng.util.dynamic.datatsource.properties;

import lombok.Data;

/**
 * 数据源配置
 * @author suwenguang
 **/
@Data
public class Datasource {

    private String driverClass;

    private String url;

    private String username;

    private String password;
}
