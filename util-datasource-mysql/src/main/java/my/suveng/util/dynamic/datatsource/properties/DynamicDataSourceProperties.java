package my.suveng.util.dynamic.datatsource.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Configuration;

/**
 *  动态数据源配置
 * @author suwenguang
 **/
@Configuration
@ConfigurationProperties(prefix = "dynamic")
@Data
public class DynamicDataSourceProperties {

    private String type;

    private String jpaPackagesToScan;

    private String mapperLocation;

    private String mapperConfigLoaction;

    @NestedConfigurationProperty
    private  Datasource master;

    @NestedConfigurationProperty
    private Datasource[] slaves;

}
