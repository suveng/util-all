package my.suveng.util.dynamic.datatsource.annotations;

/**
 * 数据库角色
 * @author suwenguang
 **/
public enum DatasourceRoleType {
    //
    master("master"),
    slave("slave");
    private final String name;

    DatasourceRoleType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
