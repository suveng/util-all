package my.suveng.util.dynamic.datatsource.route;

import java.util.ArrayList;
import java.util.List;

/**
 * 数据源轮询
 * @author suwenguang
 **/
public class DataSourcePolling {
	public static final List<String> DATASOURCES = new ArrayList<>(2);

	/**
	 * 计数值
	 */
	private static int incr = 0;

	/**
	 * 最大值
	 */
	private static final int MAX = Integer.MAX_VALUE / 1000;



	/**
	 * 自动轮询,只轮询从库
	 * @author suwenguang
	 */
	public static String autoPolling() {
		//判定是否需要重置
		reset();
		incr++;
		int i =  (incr % (DATASOURCES.size() - 1)) + 1;
		return DATASOURCES.get(i);
	}


	/**
	 * 重置指数
	 * @author suwenguang
	 */
	private static void reset() {
		if (incr > MAX) {
			incr = 1;
		}
	}
}
