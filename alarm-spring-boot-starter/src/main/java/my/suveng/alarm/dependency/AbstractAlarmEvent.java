package my.suveng.alarm.dependency;

import my.suveng.alarm.dependency.Message;
import org.springframework.context.ApplicationEvent;

/**
 * 告警事件
 * @author suwenguang
 * @date 2020/11/4 9:03 下午
 * @since 1.0
 */
public abstract class AbstractAlarmEvent extends ApplicationEvent {
    /**
     * 内容
     */
    protected String content;

    /**
     * 将alarmEvent 转化成 Message
     */
    public abstract Message tranfer();

    public AbstractAlarmEvent(String content) {
        super(content);
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "AbstractAlarmEvent{" +
                "content='" + content + '\'' +
                '}';
    }
}
