package my.suveng.alarm.dependency;

/**
 * @author suwenguang
 * @date 2020/11/7 8:28 下午
 * @since 1.0
 */
public interface Notify {
    void send(Message message);
}
