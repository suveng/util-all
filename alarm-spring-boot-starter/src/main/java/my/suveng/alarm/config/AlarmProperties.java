package my.suveng.alarm.config;

import my.suveng.alarm.AlarmListener;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @author suwenguang
 * @date 2020/11/7 8:31 下午
 * @since 1.0
 */
@Configuration
@ConfigurationProperties(prefix = "alarm")
@ConditionalOnClass(AlarmListener.class)
@EnableConfigurationProperties(AlarmProperties.class)
public class AlarmProperties {
    private String dingRoboLink;

    private boolean logEnable;

    private int restConnectTimeout;

    private int restReadTimeout;

    private List<String> phones;

    public List<String> getPhones() {
        return phones;
    }

    public void setPhones(List<String> phones) {
        this.phones = phones;
    }

    public int getRestConnectTimeout() {
        return restConnectTimeout;
    }

    public void setRestConnectTimeout(int restConnectTimeout) {
        this.restConnectTimeout = restConnectTimeout;
    }

    public int getRestReadTimeout() {
        return restReadTimeout;
    }

    public void setRestReadTimeout(int restReadTimeout) {
        this.restReadTimeout = restReadTimeout;
    }

    public boolean isLogEnable() {
        return logEnable;
    }

    public void setLogEnable(boolean logEnable) {
        this.logEnable = logEnable;
    }

    public String getDingRoboLink() {
        return dingRoboLink;
    }

    public void setDingRoboLink(String dingRoboLink) {
        this.dingRoboLink = dingRoboLink;
    }
}
