package my.suveng.alarm.de;

import cn.hutool.core.util.ObjectUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author suwenguang
 * @date 2020/11/7 9:00 下午
 * @since 1.0
 */
public class AlarmRejectHandler implements RejectedExecutionHandler {

    private static final Logger log = LoggerFactory.getLogger(AlarmRejectHandler.class);

    @Override
    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
        String taskName = "";
        if (ObjectUtil.isNotEmpty(r) && ObjectUtil.isNotEmpty(r.getClass()) && ObjectUtil.isNotEmpty(r.getClass().getName())) {
            taskName = r.getClass().getName();
        }
        log.error("任务过多, 降级为日志, 任务类名: {} ", taskName);
    }
}
