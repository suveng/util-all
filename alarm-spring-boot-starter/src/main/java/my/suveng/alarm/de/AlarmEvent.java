package my.suveng.alarm.de;

import my.suveng.alarm.dependency.AbstractAlarmEvent;
import my.suveng.alarm.dependency.Message;

/**
 * @author suwenguang
 * @date 2020/11/8 2:30 下午
 * @since 1.0
 */
public class AlarmEvent extends AbstractAlarmEvent {

    public AlarmEvent(String content) {

        super(content);
    }

    @Override
    public Message tranfer() {
        return new DeMessage(this.content);
    }
}
