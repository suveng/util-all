package my.suveng.alarm.de;

import my.suveng.alarm.dependency.Message;

/**
 * @author suwenguang
 * @date 2020/11/8 2:41 下午
 * @since 1.0
 */
public class DeMessage implements Message {

    private String content;


    public DeMessage(String  content) {
        this.content = content;
    }

    @Override
    public String beautiful() {
        return null;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
